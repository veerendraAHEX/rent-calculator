var data = [{
  "sale": "150000",
  "year": "2",
  "color": "green"
}, {
  "sale": "153800",
  "year": "4",
  "color": "green"
}, {
  "sale": "159000",
  "year": "6",
  "color": "green"
}, {
  "sale": "166000",
  "year": "8",
  "color": "green"
}, {
  "sale": "173900",
  "year": "10",
  "color": "green"
}, {
  "sale": "181999",
  "year": "12",
  "color": "green"
}];


var data2 = [{
  "sale": "150000",
  "year": "2",
  "color": "blue"
}, {
  "sale": "151700",
  "year": "4",
  "color": "blue"
}, {
  "sale": "153800",
  "year": "6",
  "color": "blue"
}, {
  "sale": "157000",
  "year": "8",
  "color": "blue"
}, {
  "sale": "161999",
  "year": "10",
  "color": "blue"
}, {
  "sale": "167000",
  "year": "12",
  "color": "blue"
}];

var myTickFormat = function (d) { //Logic to reduce big numbers
  var limits = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000];
  var shorteners = ['Q', 'T', 'B', 'M', 'K'];
  for (var i in limits) {
    if (d > limits[i]) {
      return "$" + (d / limits[i]).toFixed() + shorteners[i];
    }
  }
  return d;
};

function onChange(no) {
  if (no == 1) {
      data = data.map((obj,i)=>{
         if(i > 0)
         obj.sale =  Number(obj.sale)+i*800;
         else
         obj.sale =  Number(obj.sale)+10;
        return obj ;  
        //  return obj
      })
      console.log(data);
    // data[3].sale = Math.floor(Math.random() * (167000 - 166000 + 1) + 166000);
    // data[4].sale = Math.floor(Math.random() * (174900 - 173900 + 1) + 173900);
    // data[5].sale = Math.floor(Math.random() * (182999 - 181999 + 1) + 181999);
    updateChart(data,data2);
  } else if (no == 2) {
    data = data.map((obj,i)=>{
        if(i > 0)
        obj.sale =  Number(obj.sale)-i*400;
        else
        obj.sale =  Number(obj.sale)-10;
       return obj ;  
       //  return obj
     })
    // data[3].sale = Math.floor(Math.random() * (155000 - 166000 + 1) + 166000);
    // data[4].sale = Math.floor(Math.random() * (174900 - 173900 + 1) + 173900);
    // data[5].sale = Math.floor(Math.random() * (182999 - 181999 + 1) + 181999);
    updateChart(data,data2);
  } else if (no == 3) {
    // data2[0].sale = Math.floor(Math.random() * (189900 - 183000 + 1) + 183000);
    data2 = data2.map((obj,i)=>{
        if(i > 0)
        obj.sale =  Number(obj.sale)+i*1000;
        else
        obj.sale =  Number(obj.sale)+30;
       return obj ;  
       //  return obj
     })
     updateChart(data,data2);
  }
}

var vis = d3.select("#visualisation"),
  WIDTH = 700,
  HEIGHT = 500,
  MARGINS = {
    top: 20,
    right: 20,
    bottom: 20,
    left: 50
  },
  // x = d3.time.scale().range([0, WIDTH]);
  xScale = d3.scaleLinear().range([MARGINS.left, WIDTH - MARGINS.right]).domain([2, 12]),

  yScale = d3.scaleLinear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([130000, 210000]),

  // xAxis = d3.svg.axis()
  // .scale(xScale),

  xAxis = d3.axisBottom().scale(xScale);

yAxis = d3.axisLeft()
  .scale(yScale)
  .tickFormat(function (d, i) {
    return myTickFormat(Number(d));
  })
// .orient("left");


var lineGen = d3.line()
  .x(function (d) {
    return xScale(d.year);
  })
  .y(function (d) {
    return yScale(Number(d.sale));
  })
// .interpolate("basis");

vis.append("svg:g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")")
  .call(xAxis);

vis.append("svg:g")
  .attr("class", "y axis")
  .attr("transform", "translate(" + (MARGINS.left) + ",0)")
  .call(yAxis);




function updateChart(data,data2) {
  console.log("updateChart", data,data2)
  // d3.select("#visualisation").selectAll("*").remove();  

  var u = d3.select("#visualisation").selectAll(".blueLine")
    //  .data(data);
    .data([data], function (d, i) {
      console.log(d);
      return d.sale
    });

    var i = d3.select("#visualisation").selectAll(".greenLine")
    //  .data(data);
    .data([data2], function (d, i) {
      console.log(d);
      return d.sale
    });

  u
    .enter()
    .append("path")
    .attr("class", "blueLine")
    .merge(u)
    .transition()
    .duration(3000)
    .attr("d", d3.line()
      .x(function (d) {
        console.log(d);
        return xScale(d.year);
      })
      .y(function (d) {
        return yScale(Number(d.sale));
      }))
    .attr("fill", "none")
    .attr("stroke", "green")
    .attr("stroke-width", 2.5)

    i
    .enter()
    .append("path")
    .attr("class", "greenLine")
    .merge(i)
    .transition()
    .duration(3000)
    .attr("d", d3.line()
      .x(function (d) {
        console.log(d);
        return xScale(d.year);
      })
      .y(function (d) {
        return yScale(Number(d.sale));
      }))
    .attr("fill", "none")
    .attr("stroke", "blue")
    .attr("stroke-width", 2.5)



  var mouseG = vis.append("g")
    .attr("class", "mouse-over-effects");

  mouseG.append("path") // this is the black vertical line to follow mouse
    .attr("class", "mouse-line")
    .style("stroke", "black")
    .style("stroke-width", "1px")
    .style("opacity", "0");

  var lines = document.getElementsByClassName('blueLine');
  var cities = [...data, ...data2];
  var mousePerLine = mouseG.selectAll('.mouse-per-line')
    .data(cities)
    .enter()
    .append("g")
    .attr("class", "mouse-per-line");
 
  mousePerLine.append("circle")
    .attr("r", 7)
    .style("stroke", function(d) {
      return d.color;
    })
    .style("fill",  function(d) {
        console.log(d);
      return d.color;
    })
    .style("stroke-width", "1px")
    .style("opacity", "0");

  // mousePerLine.append("text")
  //   .attr("transform", "translate(10,3)");

  mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
    .attr('width', WIDTH) // can't catch mouse events on a g element
    .attr('height', HEIGHT)
    .attr('fill', 'none')
    .attr('pointer-events', 'all')
    // .on('mouseout', function() { // on mouse out hide line, circles and text
    //   d3.select(".mouse-line")
    //     .style("opacity", "0");
    //   d3.selectAll(".mouse-per-line circle")
    //     .style("opacity", "0");
    //   d3.selectAll(".mouse-per-line text")
    //     .style("opacity", "0");
    // })
    // .on('mouseover', function() { // on mouse in show line, circles and text
    //   // d3.attr("class", "line")

    //   d3.select(".mouse-line")
    //   .style("stroke-dasharray", ("3, 3")) 
    //     .style("opacity", "0.5");
    //   d3.selectAll(".mouse-per-line circle")
    //     .style("opacity", "1");
    //   d3.selectAll(".mouse-per-line text")
    //     .style("opacity", "1");
    // })
    // .on('mousemove', function() { // mouse moving over canvas
    //   var mouse = d3.mouse(this);
    //   d3.select(".mouse-line")
    //     .attr("d", function() {
    //       var d = "M" + mouse[0] + "," + HEIGHT;
    //       d += " " + mouse[0] + "," + 0;
    //       return d;
    //     });

    //     // setTimeout(() => {
    //       d3.selectAll(".mouse-per-line")
    //     .attr("transform", function(d, i) {
    //       if(i < 11)
    //       {
    //       // var xDate = x.invert(mouse[0]),
    //       //     bisect = d3.bisector(function(d) { return d.year; }).right;
    //       //     idx = bisect(d, xDate);
    //       // console.log(lines[i].getTotalLength())
    //       var beginning = 0,
    //           end = lines[i%2].getTotalLength(),
    //           target = null;

    //       while (true){
    //         target = Math.floor((beginning + end) / 2);
    //         pos = lines[i%2].getPointAtLength(target);
    //         console.log(target,beginning,end,mouse[0])
    //         if ((target === end || target === beginning) && pos.x !== mouse[0]) {
    //             console.log("First Block 1")
    //             break;
    //         }
    //         if (pos.x > mouse[0])  
    //         {
    //           console.log("First Block 2")
    //               end = target;
    //         } 
    //         else if (pos.x < mouse[0])
    //         {
    //           console.log("First Block 3")
    //            beginning = target;
    //         }
    //         else
    //         {
    //           console.log("First Block 4")
    //            break; //position found
    //         } 
    //       }

    //       // d3.select(this).select('text')
    //       //   .text(y.invert(pos.y).toFixed(2));
    //       console.log(`translate(" + ${mouse[0]} + "," + ${pos.y} +")`)
    //       return "translate(" + mouse[0] + "," + pos.y +")";
    //     }
    //   });

    // })
    // InitChart();
        // }, 5000);


  // .attr("transform", "translate(0,0) scale(1, 1)");     




}
updateChart(data,data2);